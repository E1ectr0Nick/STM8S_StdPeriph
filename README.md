  
File: en.stsw-stm8069.zip
From: https://www.st.com/en/embedded-software/stsw-stm8069.html

---------------------------------------------------------------


|

# Release Notes for STM8S/A Standard Peripherals Library (StdPeriph_Lib)  

Copyright © 2014 STMicroelectronics

![](_htmresc/logo.bmp)  
  
---  
  


## Contents

  1. STM8S/A Standard Peripherals Library update history
  2. License

## STM8S/A Standard Peripherals Library update history

### V2.3.1 / 26-April-2018

 ** _Main Changes_**

  * Adapt CAN_Network example to fix @svlreg COSMIC linker error.

 ** _Contents_**

  * STM8S_StdPeriph_Driver V2.3.0 ([release notes](Libraries/STM8S_StdPeriph_Driver/Release_Notes.html))
  * STM8S_StdPeriph_Examples V2.0.4 ([release notes](Project/STM8S_StdPeriph_Examples/Release_Notes.html))
  * STM8S_StdPeriph_Template V2.3.0 ([release notes](Project/STM8S_StdPeriph_Template/Release_Notes.html))
  * STM8S_EVAL V1.0.1 ([release notes](Utilities/STM8S_EVAL/Release_Notes.html))

 ** _Development Toolchains_**

  * ST Visual Develop (STVD) software toolchain

    * STVD Version 4.3.12
    * Cosmic STM8 32K compiler Version 4.4.7
    * Raisonance STM8/ST7 C compiler Version 2.60.16.0273
  * Raisonance IDE RIDE7 (RIDE) software toolchain
    * Version: RIDE7 IDE:7.66.17.0101, RKit-STM8 for 2.60.16.0273
  * IAR Embedded Workbench for STM8 IDE (EWSTM8) software toolchain
    * Version v3.30.1

      **_Known Limitations  
_**

  * The EWSTM8 toolchain compiler generate compilation warning on stm8s_itc.c

### V2.3.0 / 16-June-2017

 ** _Main Changes_**

  * Add the support of the STM8S001 8K (STM8S001J3) product family devices.

 ** _Contents_**

  * STM8S_StdPeriph_Driver V2.3.0 ([release notes](Libraries/STM8S_StdPeriph_Driver/Release_Notes.html))
  * STM8S_StdPeriph_Examples V2.0.3 ([release notes](Project/STM8S_StdPeriph_Examples/Release_Notes.html))
  * STM8S_StdPeriph_Template V2.3.0 ([release notes](Project/STM8S_StdPeriph_Template/Release_Notes.html))
  * STM8S_EVAL V1.0.1 ([release notes](Utilities/STM8S_EVAL/Release_Notes.html))

 ** _Development Toolchains_**

  * ST Visual Develop (STVD) software toolchain

    * STVD Version 4.3.11
    * Cosmic STM8 32K compiler Version 4.4.6
    * Raisonance STM8/ST7 C compiler Version 2.60.16.0273
  * Raisonance IDE RIDE7 (RIDE) software toolchain
    * Version: RIDE7 IDE:7.66.17.0101, RKit-STM8 for 2.60.16.0273
  * IAR Embedded Workbench for STM8 IDE (EWSTM8) software toolchain
    * Version v3.30.1

      **_Known Limitations  
_**

  * The EWSTM8 toolchain compiler generate compilation warning on stm8s_itc.c

### V2.2.0 / 30-September-2014

 ** _Main Changes_**

  * Add the support of the STM8AF 8K (STM8AF622x) product family devices.
  * Update License disclaimer in all package files to use MCD-ST Liberty SW License Agreement V2.

 ** _Contents_**

  * STM8S_StdPeriph_Driver V2.2.0 ([release notes](Libraries/STM8S_StdPeriph_Driver/Release_Notes.html))
  * STM8S_StdPeriph_Examples V2.0.2 ([release notes](Project/STM8S_StdPeriph_Examples/Release_Notes.html))
  * STM8S_StdPeriph_Template V2.2.0 ([release notes](Project/STM8S_StdPeriph_Template/Release_Notes.html))
  * STM8S_EVAL V1.0.1 ([release notes](Utilities/STM8S_EVAL/Release_Notes.html))

  

 ** _Development Toolchains_**

  * ST Visual Develop (STVD) software toolchain

    * STVD Version 4.3.6
    * Cosmic STM8 32K compiler Version 4.3.12 (or later)
    * Raisonance STM8/ST7 C compiler Version 2.52.13.0324
  * Raisonance IDE RIDE7 (RIDE) software toolchain
    * Version: RIDE7 IDE:7.48.13.0324, RKit-STM8 for 2.52.13.0324
  * IAR Embedded Workbench for STM8 IDE (EWSTM8) software toolchain
    * Version v1.42.1

### V2.0.0 / 25-February-2011

 ** _Main Changes_**

  * Add the support of the STM8A product family devices.
  * Add IAR Embedded Workbench for STM8 (EWSTM8) toolchain support.
  * New peripheral library architecture
  * New "Utilities" folder and drivers architecture 

  

**_Contents_**

  * STM8S_StdPeriph_Driver V2.0.0 ([release notes](Libraries/STM8S_StdPeriph_Driver/Release_Notes.html))
  * STM8S_StdPeriph_Examples V2.0.0 ([release notes](Project/STM8S_StdPeriph_Examples/Release_Notes.html))
  * STM8S_StdPeriph_Template V2.0.0 ([release notes](Project/Template/Release_Notes.html))
  * STM8S_EVAL V1.0.0 ([release notes](Utilities/STM8S_EVAL/Release_Notes.html))

  

**_Development Toolchains_**

  * ST Visual Develop (STVD) software toolchain
    * STVD Version 4.2.0
    * Cosmic STM8 32K compiler Version 4.3.5 (or later)
    * Raisonance STM8/ST7 C compiler Version 2.32.10.0307
  * Raisonance IDE RIDE7 (RIDE) software toolchain
    * Version: RIDE7 IDE:7.30.10.0169, RKit-STM8 for  2.32.10.0307
  * IAR Embedded Workbench for STM8 IDE (EWSTM8) software toolchain
    * Version v1.20

## License

censed under MCD-ST Liberty SW License Agreement V2, (the "License"); You may
not use this package except in compliance with the License. You may obtain a
copy of the License at:  
  

       <http://www.st.com/software_license_agreement_liberty_v2>  

  
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
the License for the specific language governing permissions and limitations
under the License.

* * *

For complete documentation on STM8S 8-bit microcontrollers platform visit
[](http://www.st.com/mcu/inchtml-pages-stm8l.html)[
_www.st.com_](http://www.st.com/st-web-
ui/active/en/catalog/mmc/FM141/SC1544/SS1375)  
---  
  


